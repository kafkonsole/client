// Copyright (c) 2019 Timur Sultanaev
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package consumer

import (
	"fmt"
	"log"

	"nanomsg.org/go/mangos/v2/protocol/req"
)

type ConsumeRequest struct {
	number int64
	port   int
	host   string
}

func NewConsumeRequest(number int64) *ConsumeRequest {
	return &ConsumeRequest{
		number: number,
	}
}

func (creq *ConsumeRequest) reqClient() error {

	sock, e := req.NewSocket()
	if e != nil {
		return e
	}
	defer sock.Close()

	url := fmt.Sprintf("ws://%s:%d/consumers", creq.host, creq.port)

	log.Printf("Start dial to %s", url)
	if e = sock.Dial(url); e != nil {
		return e
	}
	log.Printf("Dialed")

	log.Printf("Start sending Hello")
	if e = sock.Send([]byte("Hello")); e != nil {
		return e
	}

	log.Printf("Start waiting for reply")
	if m, e := sock.Recv(); e != nil {
		return e
	} else {
		log.Printf("Received reply")
		msg := string(m)
		log.Println(msg)
	}
	return nil
}

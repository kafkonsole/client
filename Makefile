UI_DIR=../kafkonsole-ui
SERVER_DIR=../kafkonsole-server

all: build copy-wasm-js

build-wasm:
	GOARCH=wasm GOOS=js go build -o main.wasm

copy-wasm-js:
	cp "$$(go env GOROOT)/misc/wasm/wasm_exec.js" ${UI_DIR}/public/wasm_exec.js

copy:
	cp main.wasm ${SERVER_DIR}/ui/build/main.wasm
	cp main.wasm ${UI_DIR}/public/main.wasm

.PHONY: build
build: build-wasm copy 

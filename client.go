// Copyright (c) 2019 Timur Sultanaev
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package main

import (
	"encoding/json"
	"strconv"
	"strings"

	"github.com/gopherjs/gopherwasm/js"
	log "github.com/sirupsen/logrus"
	wasm "github.com/strowk/mangos-in-browser/client/wasm"
	"gitlab.com/kafkonsole/client/endpoint"
	"gitlab.com/kafkonsole/client/topics"
)

func defaultLocationPort() int {
	protoString := js.Global().Get("window").Get("location").Get("protocol")
	schema := protoString.String()
	if strings.HasSuffix(schema, ":") {
		schema = strings.TrimSuffix(schema, ":")
	}
	if schema == "https" {
		return 443
	}
	return 80
}

func locationPort() int {
	portString := js.Global().Get("window").Get("location").Get("port")
	if portString != js.Undefined() {
		port, err := strconv.Atoi(portString.String())
		if err != nil {
			log.Errorf("Failed to parse window.location.port: %v", err)
			return defaultLocationPort()
		}
		return port
	}
	log.Errorf("Empty window.location.port")
	return defaultLocationPort()
}

func locationHost() string {
	hostString := js.Global().Get("window").Get("location").Get("hostname")
	if hostString != js.Undefined() {
		return hostString.String()
	}
	log.Panicf("Empty window.location.hostname")
	return ""
}

type ClientConfig struct {
	Hostname *string `json:"hostname"`
	Port     *int    `json:"port"`
}

func getConfig() *ClientConfig {
	confVal := js.Global().Get("document").Get("_clientStore").Get("config")
	confJson := js.Global().Get("JSON").Call("stringify", confVal)
	cfg := &ClientConfig{}
	err := json.Unmarshal([]byte(confJson.String()), cfg)
	if err != nil {
		log.Panicf("Failed to parse config %v", err)
	}

	if cfg.Hostname == nil {
		host := locationHost()
		cfg.Hostname = &host
	}
	if cfg.Port == nil {
		port := locationPort()
		cfg.Port = &port
	}
	return cfg
}

func main() {
	log.SetLevel(log.DebugLevel)

	cfg := getConfig()
	port := *cfg.Port
	host := *cfg.Hostname

	wasm.Init()
	point := endpoint.KafkonsolePoint{
		Host: host,
		Port: port,
	}
	topicsRequester, err := topics.NewTopicsRequester(point)
	if err != nil {
		log.Panicf("Failed to establish connection to request topics")
	}
	defer topicsRequester.Close()

	api := make(map[string]interface{})

	lt := js.NewCallback(func(args []js.Value) {
		topicsRequester.GetAndSet()
	})
	defer lt.Release()
	api["loadTopics"] = lt

	ct := js.NewCallback(func(args []js.Value) {
		name := args[0].String()
		topicsRequester.CreateTopic(name)
	})
	defer ct.Release()
	api["createTopic"] = ct

	dt := js.NewCallback(func(args []js.Value) {
		names := make([]string, len(args))
		for i, arg := range args {
			names[i] = arg.String()
		}
		topicsRequester.DeleteTopics(names)
	})
	defer dt.Release()
	api["deleteTopics"] = dt

	sb := js.NewCallback(func(args []js.Value) {
		topic := args[0].String()
		log.Printf("Subscribing to %s", topic)
		topicsRequester.Subscribe(topic)
	})
	defer sb.Release()
	api["subscribe"] = sb

	cm := js.NewCallback(func(args []js.Value) {
		id := args[0].String()
		log.Printf("Consuming from %s", id)
		topicsRequester.Consume(id)
	})
	defer cm.Release()
	api["consume"] = cm

	pd := js.NewCallback(func(args []js.Value) {
		topic := args[0].String()
		log.Printf("Producing to %s", topic)

		leng := args[1].Get("byteLength").Int()
		data := make([]byte, leng)
		strBytesRep := args[1].String()
		for i, strByte := range strings.Split(strBytesRep, ",") {
			numi, err := strconv.Atoi(strByte)
			if err != nil {
				log.Errorf("Failed to decode %s", strByte)
				return
			}
			data[i] = byte(numi)
		}

		topicsRequester.Produce(topic, data)
	})
	defer cm.Release()
	api["produce"] = pd

	js.Global().Get("document").Get("_clientStore").Set("client", api)

	select {}
}

// Copyright (c) 2019 Timur Sultanaev
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package topics

import (
	"encoding/binary"

	"gitlab.com/kafkonsole/client/protos"
)

func num(method string) []byte {
	req := make([]byte, 4)
	num := protos.TopicsRequestMethod_value[method]
	binary.LittleEndian.PutUint32(req, uint32(num))
	return req
}

func list() []byte {
	return num(protos.TopicsRequestMethod_LIST.String())
}

func create() []byte {
	return num(protos.TopicsRequestMethod_CREATE.String())
}

func delete() []byte {
	return num(protos.TopicsRequestMethod_DELETE.String())
}

func subscribe() []byte {
	return num(protos.TopicsRequestMethod_SUBSCRIBE.String())
}

func consume() []byte {
	return num(protos.TopicsRequestMethod_CONSUME.String())
}

func produce() []byte {
	return num(protos.TopicsRequestMethod_PRODUCE.String())
}

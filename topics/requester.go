// Copyright (c) 2019 Timur Sultanaev
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package topics

import (
	"fmt"

	"github.com/golang/protobuf/proto"
	"github.com/gopherjs/gopherwasm/js"
	log "github.com/sirupsen/logrus"
	"gitlab.com/kafkonsole/client/endpoint"
	"gitlab.com/kafkonsole/client/protos"
	"gitlab.com/kafkonsole/client/requester"
	"nanomsg.org/go/mangos/v2"
	"nanomsg.org/go/mangos/v2/protocol"
	"nanomsg.org/go/mangos/v2/protocol/req"
)

type TopicsRequester struct {
	topicsBarrier    *requester.Barrier
	consumingBarrier *requester.Barrier
	endpoint         endpoint.KafkonsolePoint
	sock             *protocol.Socket
}

func NewTopicsRequester(endpoint endpoint.KafkonsolePoint) (*TopicsRequester, error) {
	sock, e := req.NewSocket()

	if e != nil {
		return nil, e
	}

	url := fmt.Sprintf("ws://%s:%d/topics", endpoint.Host, endpoint.Port)

	log.Printf("Start dial to %s", url)
	if e = sock.Dial(url); e != nil {
		return nil, e
	}
	log.Printf("Dialed")

	consumingBarrier := &requester.Barrier{
		GetContext: func() mangos.Context {
			ctx, err := sock.OpenContext()
			if err != nil {
				log.WithError(err).Panic("Failed to open context")
			}
			return ctx
		},
		Par: 5,
		In:  make(chan func(mangos.Context), 5),
	}
	consumingBarrier.Run()

	topicsBarrier := &requester.Barrier{
		GetContext: func() mangos.Context {
			ctx, err := sock.OpenContext()
			if err != nil {
				log.WithError(err).Panic("Failed to open context")
			}
			return ctx
		},
		Par: 1,
		In:  make(chan func(mangos.Context), 5),
	}
	topicsBarrier.Run()

	return &TopicsRequester{
		topicsBarrier:    topicsBarrier,
		consumingBarrier: consumingBarrier,
		endpoint:         endpoint,
		sock:             &sock,
	}, nil
}

func (treq *TopicsRequester) Close() {
	(*treq.sock).Close()
}

func (treq *TopicsRequester) GetAndSet() {
	treq.topicsBarrier.In <- func(ctx mangos.Context) {
		treq.getAndSet(ctx) // TODO: propogate error to UI
	}
}

func (treq *TopicsRequester) CreateTopic(name string) {
	treq.topicsBarrier.In <- func(ctx mangos.Context) {
		treq.createTopic(name, ctx) // TODO: propogate error to UI
	}
}

func (treq *TopicsRequester) DeleteTopics(names []string) {
	treq.topicsBarrier.In <- func(ctx mangos.Context) {
		treq.deleteTopics(names, ctx) // TODO: propogate error to UI
	}
}

func (treq *TopicsRequester) Subscribe(topic string) {
	treq.consumingBarrier.In <- func(ctx mangos.Context) {
		treq.subscribe(topic, ctx) // TODO: propogate error to UI
	}
}

func (treq *TopicsRequester) Consume(id string) {
	treq.consumingBarrier.In <- func(ctx mangos.Context) {
		treq.consume(id, ctx) // TODO: propogate error to UI
	}
}

func (treq *TopicsRequester) Produce(topic string, data []byte) {
	treq.topicsBarrier.In <- func(ctx mangos.Context) {
		treq.produce(topic, data, ctx) // TODO: propogate error to UI
	}
}

func (treq *TopicsRequester) produce(topic string, data []byte, ctx mangos.Context) error {
	sock := ctx
	req := &protos.TopicProduceRequest{
		Topic: topic,
		Value: data,
	}
	msg, err := proto.Marshal(req)
	if err != nil {
		return err
	}

	if err = sock.Send(append(produce(), msg...)); err != nil {
		return err
	}

	if res, err := sock.Recv(); err != nil {
		return err
	} else {
		var resp protos.TopicProduceResponse
		err := proto.Unmarshal(res, &resp)
		if err != nil {
			log.WithField("topic", topic).
				WithField("response", res).
				Error("Failed to unmarshall produce response")
			return err
		}
		log.WithField("topic", resp.GetTopic()).
			WithField("partition", resp.GetPartition()).
			WithField("offset", resp.GetOffset()).
			Debug("Received produce response")
	}
	return nil
}

func (treq *TopicsRequester) consume(id string, ctx mangos.Context) error {
	sock := ctx

	req := &protos.TopicSubsciptionConsumeRequest{
		Subscription: id,
	}
	msg, err := proto.Marshal(req)
	if err != nil {
		return err
	}

	if e := sock.Send(append(consume(), msg...)); e != nil {
		return e
	}

	if res, e := sock.Recv(); e != nil {
		return e
	} else {
		var resp protos.TopicSubsciptionConsumeResponse
		e := proto.Unmarshal(res, &resp)
		if e != nil {
			return e
		}

		recs := resp.GetRecords()
		doc := js.Global().Get("document")
		store := doc.Get("_topicsStore")
		topics := store.Get("topics")
		// topic := topics.Get(resp.GetTopic())
		topic := topics.Call("get", resp.GetTopic())
		log.Printf("checking if %s exists locally",
			resp.GetTopic(),
		)
		if topic != js.Undefined() {
			log.Printf("writing %d messages to subscription %s of topic %s",
				len(recs),
				resp.GetSubscription(),
				resp.GetTopic(),
			)

			subob := topic.Get("subscription")
			if subob != js.Undefined() {
				existing := subob.Get("records")
				if existing == js.Undefined() {
					records := js.Global().Get("Array").New()
					for _, rec := range recs {
						recob := js.Global().Get("Object").New()
						recob.Set("offset", rec.GetOffset())
						recob.Set("partition", rec.GetPartition())
						recob.Set("textValue", string(rec.GetValue()))
						records.Call("push", recob)
					}
					subob.Set("records", records)
				} else {
					records := make([]interface{}, 0)
					for _, rec := range recs {
						recob := js.Global().Get("Object").New()
						recob.Set("offset", rec.GetOffset())
						recob.Set("partition", rec.GetPartition())
						recob.Set("textValue", string(rec.GetValue()))
						records = append(records, recob)
					}
					existing.Call("push", records...)
				}

			}
		}
	}
	return nil
}

type Subscription struct {
	id string
	// stop chan bool
}

var (
	subscriptions = make(map[string]*Subscription)
)

func (treq *TopicsRequester) subscribe(name string, ctx mangos.Context) error {
	sock := ctx
	req := &protos.TopicSubscribeRequest{
		Topic: name,
	}
	msg, err := proto.Marshal(req)
	if err != nil {
		return err
	}

	if e := sock.Send(append(subscribe(), msg...)); e != nil {
		return e
	}

	if res, e := sock.Recv(); e != nil {
		return e
	} else {
		var resp protos.TopicSubscribeResponse
		e := proto.Unmarshal(res, &resp)
		if e != nil {
			return e
		}
		id := resp.GetSubscription()
		subs := &Subscription{
			id: id,
			// stop: make(chan bool),
		}
		subscriptions[id] = subs

		doc := js.Global().Get("document")
		store := doc.Get("_topicsStore")
		topics := store.Get("topics")
		topic := topics.Call("get", resp.GetTopic())
		if topic != js.Undefined() {
			log.Printf("Setting subscription %s for %s", id, resp.GetTopic())
			subob := js.Global().Get("Object").New()
			subob.Set("id", id)
			// topic.Set("subscription", subob)
			topic.Set("subscription", subob)
		}

		// go func(){

		// 	for{
		// 		select {
		// 		case <- subs.stop:
		// 			log.Printf("Stopping subscription %s", subs.id)

		// 		}
		// 	}
		// }
	}

	return nil
}

func (treq *TopicsRequester) deleteTopics(names []string, ctx mangos.Context) error {
	sock := ctx
	req := &protos.TopicsDeleteRequest{
		Names: names,
	}
	msg, err := proto.Marshal(req)
	if err != nil {
		return err
	}

	if e := sock.Send(append(delete(), msg...)); e != nil {
		return e
	}

	if res, e := sock.Recv(); e != nil {
		return e
	} else {
		log.Printf(string(res)) // TODO add error handling on response
	}

	return nil
}

func (treq *TopicsRequester) getAndSet(ctx mangos.Context) error {
	res, err := treq.getTopics(ctx)
	if err != nil {
		log.Printf("Failed to get topics: %v", err)
		return err
	}

	doc := js.Global().Get("document")
	store := doc.Get("_topicsStore")
	// topics := js.Global().Get("Array").New()
	// for _, topic := range res.Topics {
	// 	ob := js.Global().Get("Object").New()
	// 	ob.Set("name", topic.GetName())
	// 	topics.Call("push", ob)
	// }
	// store.Set("topics", topics)

	// topics := js.Global().Get("Object").New()
	// for _, topic := range res.Topics {
	// 	ob := js.Global().Get("Object").New()
	// 	ob.Set("name", topic.GetName())
	// 	topics.Set(topic.GetName(), ob)
	// }
	// store.Set("topics", topics)
	topics := store.Get("topics")
	names := make(map[string]string)
	for _, topic := range res.Topics {
		names[topic.Name] = topic.Name
		ob := js.Global().Get("Object").New()
		ob.Set("name", topic.GetName())
		topics.Call("set", topic.GetName(), ob)
	}
	keysIter := topics.Call("keys")
	var forRemoval []string
	for {
		nextKey := keysIter.Call("next")
		if nextKey.Get("done").Bool() {
			break
		}
		topicName := nextKey.Get("value").String()
		if _, found := names[topicName]; !found {
			forRemoval = append(forRemoval, topicName)
		}
	}
	for _, removingName := range forRemoval {
		topics.Call("delete", removingName)
	}

	return nil
}

func (treq *TopicsRequester) createTopic(name string, ctx mangos.Context) error {
	sock := ctx
	req := &protos.TopicCreateRequest{
		Name: name,
	}
	msg, err := proto.Marshal(req)
	if err != nil {
		return err
	}

	if e := sock.Send(append(create(), msg...)); e != nil {
		return e
	}

	if res, e := sock.Recv(); e != nil {
		log.Printf(string(res)) // TODO add error handling on response
		return e
	}
	return nil
}

func (treq *TopicsRequester) getTopics(ctx mangos.Context) (*protos.Topics, error) {
	sock := ctx

	if e := sock.Send(list()); e != nil {
		return nil, e
	}

	if m, e := sock.Recv(); e != nil {
		return nil, e
	} else {
		topics := protos.Topics{}
		e = proto.Unmarshal(m, &topics)
		if e != nil {
			return nil, e
		}
		return &topics, nil
	}
}

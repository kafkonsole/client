module gitlab.com/kafkonsole/client

go 1.12

require (
	github.com/eapache/go-resiliency v1.2.0 // indirect
	github.com/eapache/queue v1.1.0 // indirect
	github.com/golang/protobuf v1.3.2
	github.com/gopherjs/gopherwasm v1.1.0
	github.com/gorilla/mux v1.7.3 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/rcrowley/go-metrics v0.0.0-20190826022208-cac0b30c2563 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/strowk/mangos-in-browser v0.0.0-20190422113001-ffee6d727789
	github.com/strowk/websocket v0.0.0-20190421212033-8945673a35fb // indirect
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553 // indirect
	nanomsg.org/go/mangos/v2 v2.0.5
)
